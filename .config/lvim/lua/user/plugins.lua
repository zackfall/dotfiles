local ft_prog = {
  'python', 'json',
  'js', 'ts', 'tsx',
  'javascript', 'typescript', 'javascriptreact', 'typescriptreact',
  'rust', 'c', 'cpp', 'h', 'hpp', 'java', 'go', 'toml'
}

lvim.plugins = {
  { "frabjous/knap" },
  { "shatur/neovim-ayu" },
  { "SirVer/ultisnips" },
  { "honza/vim-snippets" },
  { "p00f/nvim-ts-rainbow" },
  { "folke/zen-mode.nvim" },
  { "folke/twilight.nvim" },
  { "norcalli/nvim-colorizer.lua" },
  { "nvim-lua/plenary.nvim" },
  { "lvimuser/lsp-inlayhints.nvim" },
  { "windwp/nvim-ts-autotag" },
  { "iamcco/markdown-preview.nvim" },
  { "simrat39/rust-tools.nvim" },
  { "jose-elias-alvarez/typescript.nvim" },
  { "lewis6991/impatient.nvim" },
  { 'm-demare/hlargs.nvim' },
  { "saecki/crates.nvim" },
  { "folke/trouble.nvim" },
  { "tpope/vim-surround" },
  {
    "Exafunction/codeium.vim",
    ft = ft_prog,
    cmd = { "CodeIum" },
  },
  { "ethanholz/nvim-lastplace",
    event = "BufRead",
    config = function()
      require("nvim-lastplace").setup({
        lastplace_ignore_buftype = { "quickfix", "nofile", "help" },
        lastplace_ignore_filetype = {
          "gitcommit", "gitrebase", "svn", "hgcommit"
        },
        lastplace_open_folds = true
      })
    end
  },
  {
    "folke/todo-comments.nvim",
    event = "BufRead",
    config = function()
      require("todo-comments").setup()
    end,
  },
  {
    "pwntester/octo.nvim",
    config = function()
      require("octo").setup()
    end,
  },
  {
    "ray-x/lsp_signature.nvim",
    event = "BufRead",
    config = function() require "lsp_signature".on_attach() end,
  },
  -- { dir = "~/learning/my_module", name = "my_module" }
}
