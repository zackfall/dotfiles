Config { 
    font = "xft:UbuntuMono Nerd Font:weight=bold:pixelsize=12:antialias=true:hinting=true",
    bgColor = "#1a212e",
    fgColor = "#93a4c3",
    lowerOnStart = True,
    hideOnStart = False,
    allDesktops = True,
    persistent = True,
    commands = [ 
        Run Date "  %d %b %Y %H:%M " "date" 600,
        Run Com "bash" ["-c", "checkupdates | wc -l"] "updates" 3000,
        Run Cpu ["-t", " (<total>%)","-H","50","--high","red"] 150,
        Run Memory ["-t", "  <used>M (<usedratio>%)"] 150,
        Run UnsafeStdinReader
    ],
    alignSep = "}{",
    template = "<fc=#862aa1>   </fc>%UnsafeStdinReader% }{ \
        \<fc=#c75ae9> %updates% </fc>\
        \<fc=#efbd5d> %cpu% </fc>\
        \<fc=#f65866> %memory% </fc>\
        \<fc=#34bfd0> %date% </fc>"
}
